﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace FeedToDB
{
    public class RawDB
    {
        private const int BufferSize = 1048576;
        private static readonly char[] SeperatorChar = {Convert.ToChar(255)};
        private static string _connStr;

        public static void WriteDb(string message, string timer)
        {
            var par = new ParallelOptions();
            par.MaxDegreeOfParallelism = Environment.ProcessorCount/2;
            Parallel.ForEach(message.Split(SeperatorChar, StringSplitOptions.RemoveEmptyEntries), current =>
            {
                try
                {
                    using (var connection = new MySqlConnection(_connStr))
                    {
                        connection.Open();
                        try
                        {
                            int pos = current.IndexOf(";", StringComparison.Ordinal);
                            if (pos < 1) return;

                            string msgId = current.Substring(0, pos);
                            string type = current.Substring(pos + 1, 1);
                            string totSql = string.Format(
                                "INSERT INTO `test`.`rawmsg` VALUES('{0}','{1}','{2}','{3}','{4}',CURRENT_TIMESTAMP(6));\r\n",
                                Guid.NewGuid(), msgId, type, current.Substring(pos + 2),
                                timer
                                );

                            using (var command = new MySqlCommand(totSql, connection))
                            {
                                command.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }
                }
                catch(MySqlException ex)
                {
                    Console.WriteLine("Error connecting to the server: " + ex.Message);
                }
            });
        }

        public static void StartClient()
        {
            // Data buffer for incoming data.
            var bytes = new byte[BufferSize];

            // Connect to a remote device.
            try
            {
                // Establish the remote endpoint for the socket.
                // This example uses port 11000 on the local computer.
                IPAddress[] ipHostInfo = Dns.GetHostAddresses(ConfigurationManager.AppSettings["HostName"]);
                IPAddress ipAddress = ipHostInfo[0];
                var remoteEp = new IPEndPoint(ipAddress, Convert.ToInt16(ConfigurationManager.AppSettings["HostPort"]));

                // Create a TCP/IP  socket.
                var sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.
                try
                {
                    sender.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                    sender.Connect(remoteEp);

                    Console.WriteLine("Socket connected to {0}",
                        sender.RemoteEndPoint);

                    sender.Receive(bytes);

                    // Encode the data string into a byte array.
                    byte[] msg = Encoding.Default.GetBytes(ConfigurationManager.AppSettings["UserName"] + "\r\n");

                    // Send the data through the socket.
                    sender.Send(msg);
                    sender.Receive(bytes);
                    msg = Encoding.Default.GetBytes(ConfigurationManager.AppSettings["Password"] + "\r\n");

                    sender.Send(msg);
                    int bytesWritten = 0;
                    int bytesRec = sender.Receive(bytes);
                    string leftOver = String.Empty;
                    while (bytesRec > 0)
                    {
                        try
                        {
                            string str = leftOver + Encoding.Default.GetString(bytes, 0, bytesRec);
                            int last = str.LastIndexOfAny(SeperatorChar);
                            string pstr = str.Substring(0, last);

                            Parallel.Invoke(() =>
                            {
                                WriteDb(pstr,
                                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffffff"));
                            });


                            leftOver = str.Substring(last + 1);


                            bytesWritten += bytesRec;
                            bytesRec = sender.Receive(bytes);
                            Console.WriteLine("{0} bytes written.Total : {1}", bytesRec, bytesWritten);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("exp : {0}", ex);
                            sender.Shutdown(SocketShutdown.Both);
                            sender.Close();
                            throw;
                        }
                    }
                    // Release the socket.
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane);
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static int Main(String[] args)
        {
            _connStr = String.Format("server={0};user id={1}; password={2}; database=test; pooling=true",
                "localhost", ConfigurationManager.AppSettings["DBUserName"],
                ConfigurationManager.AppSettings["DBPassword"]);
            while (true)
            {
                StartClient();
                Console.WriteLine("Session Closed");
                Console.WriteLine("\r\nReopening in 15 seconds\r\n");
                Thread.Sleep(15000);
            }
        }
    }
}