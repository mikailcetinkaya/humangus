﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Redis;

namespace RedisBackEndConsumer
{
    class Program
    {
        public static IRedisClientsManager RedisManager { get; set; }
        static void Main(string[] args)
        {
            using (var redis = RedisManager.GetClient())
            {
                var redisQuestions = redis.

                object question;
                if (question.Tags == null) question.Tags = new List<string>();
                if (question.Id == default(long))
                {
                    question.Id = redisQuestions.GetNextSequence();
                    question.CreatedDate = DateTime.UtcNow;

                    //Increment the popularity for each new question tag
                    question.Tags.ForEach(tag => redis.IncrementItemInSortedSet("urn:tags", tag, 1));
                }

                redisQuestions.Store(question);
                redisQuestions.AddToRecentsList(question);
                redis.AddItemToSet("urn:user>q:" + question.UserId, question.Id.ToString());

                //Usage of tags - Populate tag => questions index for each tag
                question.Tags.ForEach(tag => redis.AddItemToSet
                ("urn:tags>q:" + tag.ToLower(), question.Id.ToString()));
            }
        }
    }
}
