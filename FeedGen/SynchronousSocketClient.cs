﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace FeedGen
{
    public class SynchronousSocketClient
    {
        private const String Topic = "feed";
        private const String RoutingKey = "stock.bist.raw";
        private const int BufferSize = 1048576;

        private static readonly char[] SeperatorChar = {Convert.ToChar(255)};

        private static SemaphoreSlim semaphore=new SemaphoreSlim(3);

        public static void Publish(string msgBlock)
        {
            var factory = new ConnectionFactory {HostName = "127.0.0.1"};

            IConnection Connection = factory.CreateConnection();
            IModel Channel = Connection.CreateModel();
            Channel.ExchangeDeclare(Topic, "topic");
            IBasicProperties s = Channel.CreateBasicProperties();
            s.SetPersistent(false);

            foreach (string current in msgBlock.Split(SeperatorChar, StringSplitOptions.RemoveEmptyEntries))
            {
                semaphore.Wait();
                int pos = current.IndexOf(";", StringComparison.Ordinal);
                if (pos < 1) continue;
                string msgId = current.Substring(0, pos);
                string type = current.Substring(pos + 1, 1);
                string fullKey = RoutingKey + (type != "N" ? ".periodic." : ".peek.") + msgId ;
                string lNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffffff");

                byte[] fmsg =
                    Encoding.Default.GetBytes(current.Substring(pos + 2) +
                                              ";" + 
                                              lNow);
                Console.WriteLine(current);
                Channel.BasicPublish(Topic, fullKey,
                    s, fmsg);

                semaphore.Release();
            }
        }


        public static void StartClient()
        {
            // Data buffer for incoming data.
            var bytes = new byte[BufferSize];
            string leftOver = string.Empty;

            // Connect to a remote device.
            try
            {
                // Establish the remote endpoint for the socket.
                // This example uses port 11000 on the local computer.
                IPAddress[] ipHostInfo = Dns.GetHostAddresses(ConfigurationManager.AppSettings["HostName"]);
                IPAddress ipAddress = ipHostInfo[0];
                var remoteEp = new IPEndPoint(ipAddress, Convert.ToInt16(ConfigurationManager.AppSettings["HostPort"]));

                // Create a TCP/IP  socket.
                var sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.
                try
                {
                    sender.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                    sender.Connect(remoteEp);

                    Console.WriteLine("Socket connected to {0}",
                        sender.RemoteEndPoint);

                    sender.Receive(bytes);

                    // Encode the data string into a byte array.
                    byte[] msg = Encoding.Default.GetBytes(ConfigurationManager.AppSettings["UserName"] + "\r\n");

                    // Send the data through the socket.
                    sender.Send(msg);
                    sender.Receive(bytes);
                    msg = Encoding.Default.GetBytes(ConfigurationManager.AppSettings["Password"] + "\r\n");

                    sender.Send(msg);
                    int bytesRec = sender.Receive(bytes);
                    int totalMessages = 0;


                    while (bytesRec > 0)

                    {
                        try
                        {
                            string str = leftOver + Encoding.Default.GetString(bytes, 0, bytesRec);
                            int last = str.LastIndexOfAny(SeperatorChar);
                            string pstr = str.Substring(0, last);

                            var dop = new ParallelOptions();
                            dop.MaxDegreeOfParallelism = Environment.ProcessorCount/8;
                            Parallel.Invoke(dop, () => Publish(pstr));

                            leftOver = str.Substring(last + 1);
                            bytesRec = sender.Receive(bytes);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("exp : {0}", ex);
                            throw;
                        }
                    }
                    // Release the socket.
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane);
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static int Main(String[] args)
        {
            while (true)
            {
                StartClient();
                Console.WriteLine("Session Closed");
                Console.WriteLine("\r\nReopening in 15 seconds\r\n");
                Thread.Sleep(15000);
            }

            return 0;
        }
    }
}