﻿using System;
using System.Configuration;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace FeedConsumer
{
    public static class ReceiveLogsTopic
    {
        private const string Topic = "feed";
        private const String RoutingKey = "stock.bist.raw";
        private static string TableName;

        public static string ConnStr;

        public static void WriteDb(string msg)
        {
            try
            {
                using (var connection = new MySqlConnection(ConnStr))
                {
                    connection.Open();
                    string sql = string.Format(
                        "INSERT INTO {1} VALUES ({0},CURRENT_TIMESTAMP(6));",
                        "' " + msg.Replace(";", "','") + "'", TableName);

                    using (var command = new MySqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error connecting to the server: " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("WriteDb general Err: " + ex.Message);
            }
        }

        public static void Main(string[] args)
        {
            string arg0;
            if (args.Length < 2)
            {
                Console.Error.WriteLine("Usage: {0} [binding_key...]",
                    Environment.GetCommandLineArgs()[0]);
                Console.WriteLine("default: 177");
                arg0 = "peek.177";
                TableName = "`test`.`msg177`";
            }
            else
            {
                arg0 = args[0];
                TableName = args[1];
            }

            ConnStr = String.Format("server={0};user id={1}; password={2}; database=test; pooling=true",
                "localhost", ConfigurationManager.AppSettings["DBUserName"],
                ConfigurationManager.AppSettings["DBPassword"]);

            var dop = new ParallelOptions();
            dop.MaxDegreeOfParallelism = Environment.ProcessorCount*2;

            var factory = new ConnectionFactory { HostName = "localhost" };
            using (IConnection connection = factory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(Topic, "topic");
                    channel.BasicQos(0, 5, true);
                    QueueDeclareOk queueName = channel.QueueDeclare("Worker." + arg0, false, false, false, null);

                    string bindingKey = RoutingKey + "." + arg0;

                    channel.QueueBind(queueName, Topic, bindingKey);


                    Console.WriteLine(" [*] Waiting for messages. " +
                                      "To exit press CTRL+C");

                    var consumer = new QueueingBasicConsumer(channel);
                    channel.BasicConsume(queueName, true, consumer);


                    while (true)
                    {
                        BasicDeliverEventArgs ea = consumer.Queue.DequeueNoWait(null)?? consumer.Queue.Dequeue();
                        Parallel.Invoke(dop ,()=> WriteDb(Encoding.Default.GetString(ea.Body).Substring(1) + ";" +
                                                           DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffffff")));
                    }
                }
            }
        }
    }
}