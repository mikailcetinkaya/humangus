﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace FeedToFile
{
    public class RawFile
    {
        private const int BufferSize = 1048576;

        public static void StartClient()
        {
            // Data buffer for incoming data.
            var bytes = new byte[BufferSize];

            // Connect to a remote device.
            try
            {
                // Establish the remote endpoint for the socket.
                // This example uses port 11000 on the local computer.
                IPAddress[] ipHostInfo = Dns.GetHostAddresses(ConfigurationManager.AppSettings["HostName"]);
                IPAddress ipAddress = ipHostInfo[0];
                var remoteEp = new IPEndPoint(ipAddress, Convert.ToInt16(ConfigurationManager.AppSettings["HostPort"]));

                // Create a TCP/IP  socket.
                var sender = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.
                try
                {
                    FileStream logs = File.Create("c:\\feed\\"+DateTime.Now.ToString("yyyyMMddHHMMSSffffff") .Replace(":", "") + ".txt");
                    sender.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                    sender.Connect(remoteEp);

                    Console.WriteLine("Socket connected to {0}",
                        sender.RemoteEndPoint);

                    sender.Receive(bytes);

                    // Encode the data string into a byte array.
                    byte[] msg = Encoding.Default.GetBytes(ConfigurationManager.AppSettings["UserName"] + "\r\n");

                    // Send the data through the socket.
                    sender.Send(msg);
                    sender.Receive(bytes);
                    msg = Encoding.Default.GetBytes(ConfigurationManager.AppSettings["Password"] + "\r\n");

                    sender.Send(msg);
                    int bytesRec = 1;
                    int bytesWritten = 0;
                    while (bytesRec > 0)
                    {
                        try
                        {
                            bytesRec = sender.Receive(bytes);
                            logs.Write(bytes, 0, bytesRec);
                            bytesWritten += bytesRec;
                            Console.WriteLine("{0} bytes written.Total : {1}", bytesRec, bytesWritten);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("exp : {0}", ex);
                            sender.Shutdown(SocketShutdown.Both);
                            sender.Close();
                            throw;
                        }
                    }
                    // Release the socket.
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane);
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static int Main(String[] args)
        {
            while (true)
            {
                StartClient();
                Console.WriteLine("Session Closed");
                Console.WriteLine("\r\nReopening in 15 seconds\r\n");
                Thread.Sleep(15000);
            }
            Console.ReadKey();
            return 0;
        }
    }
}