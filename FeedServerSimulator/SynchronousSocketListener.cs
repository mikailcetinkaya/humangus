﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace FeedServerSimulator
{
    public class SynchronousSocketListener
    {
        // Incoming data from the client.
        public const int BufferSize = 8388608;
        public static string Data;

        public static void StartListening()
        {
            // Data buffer for incoming data.
            byte[] bytes;

            // Establish the local endpoint for the socket.
            // Dns.GetHostName returns the name of the 
            // host running the application.
            IPHostEntry ipHostInfo = Dns.Resolve("localhost");
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            var localEndPoint = new IPEndPoint(ipAddress, Convert.ToInt16(ConfigurationManager.AppSettings["HostPort"]));

            // Create a TCP/IP socket.
            var listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and 
            // listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.
                while (true)
                {
                    Console.WriteLine("\r\nWaiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.
                    Socket handler = listener.Accept();
                    Data = null;


                    // An incoming connection needs to be processed.
                    try
                    {
                        handler.Send(Encoding.Default.GetBytes("enter id -"));

                        bytes = new byte[1024];

                        int bytesRec = handler.Receive(bytes);
                        Data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        Console.WriteLine("Text received : {0}", Data);

                        handler.Send(Encoding.Default.GetBytes("enter password -"));
                        bytesRec = handler.Receive(bytes);
                        Data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        Console.WriteLine("Text received : {0}", Data);

                        FileStream file = File.OpenRead(ConfigurationManager.AppSettings["MsgFile"]);
                        var fBuf = new byte[BufferSize];
                        int readFileBytes = file.Read(fBuf, 0, BufferSize);
                        int totalWritten = 0;
                        while (readFileBytes > 0)
                        {
                            handler.Send(fBuf, 0, readFileBytes, SocketFlags.None);
                            totalWritten += readFileBytes;
                            Console.WriteLine("{0} bytes Sent. Total: {1}", readFileBytes, totalWritten);

                            readFileBytes = file.Read(fBuf, 0, BufferSize);
                        }



                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    finally
                    {
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();
        }

        public static int Main(String[] args)
        {
            StartListening();
            return 0;
        }
    }
}